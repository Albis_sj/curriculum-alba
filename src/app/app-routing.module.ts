import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgendaComponent } from './components/agenda/agenda.component';
import { EditarCitaComponent } from './components/agenda/editar-cita/editar-cita.component';
import { VerCitaComponent } from './components/agenda/ver-cita/ver-cita.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { CurriculumComponent } from './components/curriculum/curriculum.component';
import { EstudiosComponent } from './components/estudios/estudios.component';
import { MainComponent } from './components/main/main.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PortafolioComponent } from './components/portafolio/portafolio.component';


const routes: Routes = [
  {path: 'curriculum', component: CurriculumComponent},
  {path: 'index', component: MainComponent},
  {path: 'estudios', component: EstudiosComponent},
  {path: 'portafolio', component: PortafolioComponent},
  
  {path: 'agenda', component: AgendaComponent},
  {path: 'editar-cita/:id', component: EditarCitaComponent},
  {path: 'ver-cita/:id', component: VerCitaComponent},
  {path: 'contacto', component: ContactoComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'index'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
