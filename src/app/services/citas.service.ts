import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CitasService {

  listacitas!: citaI[];


  constructor() { }

  agregarTabla(cita: citaI){
    if(localStorage.getItem('Citas') === null){
      this.listacitas = [];
      this.listacitas.unshift(cita);
      localStorage.setItem('Citas', JSON.stringify(this.listacitas))
    } else {
      this.listacitas = JSON.parse(localStorage.getItem('Citas') || '');
      this.listacitas.unshift(cita);
      localStorage.setItem('Citas', JSON.stringify(this.listacitas))
    }
    // console.log(this.listacitas, 'lista de agregarTablas');
  }

  agregarCita(cita: citaI) {
    this.listacitas.unshift(cita);
    // console.log(this.listacitas);
    
  }

  buscarCita(id: string): citaI{
    return this.listacitas.find(e => e.nombre=== id) || {} as citaI
  }


  obtenerLocalStorage(){
    let citaLista = JSON.parse(localStorage.getItem("Citas") || '');
    // console.log(citaLista);
    
    this.listacitas = citaLista
    // console.log(citaLista, 'obtener LocalStorage');
    return citaLista
  }


  eliminarcita(cita: citaI){
  // console.log('intento eliminar');
  // console.log(cita, 'elimiar');
  const response = confirm('Estás seguro que deseas eliminar la cita?')

  if(response){
    for(let i = 0; i < this.listacitas.length; i ++){
      if(cita == this.listacitas[i]){
        // console.log('eli');
        
        this.listacitas.splice(i, 1);
        localStorage.setItem('Citas', JSON.stringify(this.listacitas))
      }
    }
    
  }

    this.listacitas = this.listacitas.filter(data => {
        return data.nombre!== cita.nombre
    })
  }

  eliminar(nombre: string){
    this.listacitas = this.listacitas.filter(data => {
      return data.nombre !== nombre;
    });

  }

  eliminarC(nombre:string){
    // console.log('intento eliminar');
    // console.log(nombre, 'elimiar');
  
      for(let i = 0; i < this.listacitas.length; i ++){
        if(nombre == this.listacitas[i].nombre){
          // console.log('eli');
          
          this.listacitas.splice(i, 1);
          localStorage.setItem('Citas', JSON.stringify(this.listacitas))
        }
      }
      
  
      this.listacitas = this.listacitas.filter(data => {
          return data.nombre!== nombre
      })
  }

  modificarCita(cita: citaI, citaEli: string){
    this.eliminar(citaEli);
    // console.log(citaEli, 'eliminar esta cita');
    
    this.agregarTabla(cita);
    // console.log(cita, 'Agregar esta cita');
  }
}


interface citaI{
  nombre: string,
  apellido: string,
  correo: string,
  telefono: number,
  comentario: string,
  fecha: string,
  hora: string
}