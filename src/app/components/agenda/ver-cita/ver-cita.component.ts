import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CitasService } from 'src/app/services/citas.service';

@Component({
  selector: 'app-ver-cita',
  templateUrl: './ver-cita.component.html',
  styleUrls: ['./ver-cita.component.css']
})
export class VerCitaComponent implements OnInit {


  adicionar : boolean = true;
  titulo : string = 'Modificar Usuario';
  form!: FormGroup;

  constructor(private activatedRound: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              private _citaService: CitasService) { 
    this.activatedRound.params.subscribe(params => {
      const id = params['id'];
      // console.log(id, 'constructor editar-cita');

      this.form = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      apellido: ['', [Validators.required, Validators.minLength(3)]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      telefono: ['', [Validators.required, Validators.minLength(8)]],
      comentario: ['', [Validators.required, Validators.minLength(7)]],
      lugar: ['', Validators.required],
      hora: ['', Validators.required],
      fecha: ['', Validators.required]
      });

      if(id !== 'nuevo'){
        const cita = this._citaService.buscarCita(id);
        // console.log(cita, 'cita en constante encontrada');
        
        if(Object.keys(cita).length === 0){
          this.router.navigate(['/agenda']);
        }

        this.form.patchValue({
          nombre: cita.nombre,
          apellido: cita.apellido,
          correo: cita.correo,
          telefono: cita.telefono,
          comentario: cita.comentario,
          hora: cita.hora,
          fecha: cita.fecha
        });

        console.log('Buenas Tardes');
        
        this.adicionar = false;
        this.titulo = 'Modificar Usuario';
      }

    })
  }


  ngOnInit(): void {
  }
  agregarCita(): void{
    // console.log('entro');
    
  }

  Volver(): void{
    this.router.navigate(['/agenda'])
  }

}


interface citaI{
  nombre: string,
  apellido: string,
  correo: string,
  telefono: number,
  comentario: string,
  fecha: string,
  hora: string
}