import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CitasService } from 'src/app/services/citas.service';

import { AgendaComponent } from '../../agenda/agenda.component'
@Component({
  selector: 'app-editar-cita',
  templateUrl: './editar-cita.component.html',
  styleUrls: ['./editar-cita.component.css']
})
export class EditarCitaComponent implements OnInit {


  adicionar : boolean = true;
  titulo : string = 'Modificar Usuario';
  form!: FormGroup;
  citaELI!: string;

  constructor(private activatedRound: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              private _citaService: CitasService) { 
    this.activatedRound.params.subscribe(params => {
      const id = params['id'];
      // console.log(id, 'constructor editar-cita');

      this.form = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      apellido: ['', [Validators.required, Validators.minLength(3)]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      telefono: ['', [Validators.required, Validators.minLength(8)]],
      comentario: ['', [Validators.required, Validators.minLength(7)]],
      lugar: ['', Validators.required],
      hora: ['', Validators.required],
      fecha: ['', Validators.required]
      });

      if(id !== 'nuevo'){
        const cita = this._citaService.buscarCita(id);
        // console.log(cita, 'cita en constante encontrada');
        
        if(Object.keys(cita).length === 0){
          this.router.navigate(['/agenda']);
        }

        this.form.patchValue({
          nombre: cita.nombre,
          apellido: cita.apellido,
          correo: cita.correo,
          telefono: cita.telefono,
          comentario: cita.comentario,
          hora: cita.hora,
          fecha: cita.fecha
        });

        // console.log('patchValue');
        
        this.adicionar = false;
        this.titulo = 'Modificar Usuario';
        this.citaELI = id
      }

    })
  }


    //GET
    get nombreNoValido(){
      return this.form.get('nombre')?.invalid && this.form.get('nombre')?.touched;
    }
  
    get apellidoNoValido(){
      return this.form.get('apellido')?.invalid && this.form.get('apellido')?.touched;
    }
  
    get correoNoValido(){
      return this.form.get('correo')?.invalid && this.form.get('correo')?.touched
    }
  
    get telefonoNoValido(){
      return this.form.get('telefono')?.invalid && this.form.get('telefono')?.touched
    }
  



  ngOnInit(): void {
  }

  insertarCita(): void {
    this.form = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      apellido: ['', [Validators.required, Validators.minLength(3)]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      telefono: ['', [Validators.required, Validators.minLength(8)]],
      comentario: ['', [Validators.required, Validators.minLength(7)]],
      hora: ['', Validators.required],
      fecha: ['', Validators.required]
    })
  }

  agregarCita(): void{
    // console.log('entro');
    

    const cita: citaI ={
      nombre: this.form.value.nombre,
      apellido: this.form.value.apellido,
      correo: this.form.value.correo,
      telefono: this.form.value.telefono,
      comentario: this.form.value.comentario,
      hora: this.form.value.hora,
      fecha: this.form.value.fecha
    }

    if(this.adicionar){
      this._citaService.agregarCita(cita);

      this.router.navigate(['/agenda']);
    }

    else {
      this._citaService.eliminar(this.citaELI)
      this._citaService.modificarCita(cita, this.citaELI);
      // console.log(cita, 'Modificar en editar-Cita');
      
      this.router.navigate(['/agenda'])
      // console.log('Ir a la Agenda');
      
    }
  }

  Volver(): void{
    this.router.navigate(['/agenda'])
  }

}


interface citaI{
  nombre: string,
  apellido: string,
  correo: string,
  telefono: number,
  comentario: string,
  fecha: string,
  hora: string
}