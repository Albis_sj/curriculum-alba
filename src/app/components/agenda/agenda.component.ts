import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { CitasService } from 'src/app/services/citas.service';


@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {

  encabezado: string[]= ['Nombre', 'Teléfono', 'Fecha', 'Hora']

  mensajeV: string = 'Su cita fue agendada';
  mensaje!: string;

  mostrarMensaje: boolean = false;

  listacitas!: citaI[];
  deshabilitarObtener : boolean= false
  mostrarTabla: boolean = false

  formulario!: FormGroup;
  constructor(private _citaService: CitasService,
              private fb: FormBuilder,
    private router: Router) {

    this.crearFormulario()

    localStorage.removeItem('nombre');

  }

  crearFormulario(): void{
    this.formulario = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      apellido: ['', [Validators.required, Validators.minLength(3)]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      telefono: ['', [Validators.required, Validators.minLength(8)]],
      comentario: ['', [Validators.required, Validators.minLength(7)]],
      lugar: ['', Validators.required],
      hora: ['', Validators.required],
      fecha: ['', Validators.required]
    });
  }

  //GET
  get nombreNoValido(){
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched;
  }

  get apellidoNoValido(){
    return this.formulario.get('apellido')?.invalid && this.formulario.get('apellido')?.touched;
  }

  get correoNoValido(){
    return this.formulario.get('correo')?.invalid && this.formulario.get('correo')?.touched
  }

  get telefonoNoValido(){
    return this.formulario.get('telefono')?.invalid && this.formulario.get('telefono')?.touched
  }


  ngOnInit(): void {
  }

  guardar(): void{
    // console.log('guardar');

    this.mensaje = this.mensajeV
    this.añadirInfo();
    
    setTimeout(() => {
      this.mensaje = ''
    }, 4000);
  }

  añadirInfo(){
    const user: citaI = {
      nombre: this.formulario.value.nombre,
      apellido: this.formulario.value.apellido,
      telefono: this.formulario.value.telefono,
      correo: this.formulario.value.correo,
      comentario: this.formulario.value.comentario,
      fecha: this.formulario.value.fecha,
      hora: this.formulario.value.hora
    }
    // console.log(user);

    this._citaService.agregarTabla(user)
    this.formulario.reset()

    this.deshabilitarObtener = true
  }

  buscarCita(id:string){
    this._citaService.buscarCita(id)
  }


  eliminar(cita: citaI){
    this._citaService.eliminarcita(cita)
  }


  modificarCita(cita: string){
    // console.log(cita, 'Modificacion');
    this.router.navigate(['/editar-cita/', cita])
  }

  verCita(cita: string){
    // console.log(cita, 'Visualizar');
    this.router.navigate(['/ver-cita/', cita])
  }

  obtenerMuestra(){
      this.listacitas = this._citaService.obtenerLocalStorage()
    // console.log(this.listacitas, 'Obtener');
      this.mostrarTabla = true
  }

}

interface citaI{
  nombre: string,
  apellido: string,
  correo: string,
  telefono: number,
  comentario: string,
  fecha: string,
  hora: string
}
