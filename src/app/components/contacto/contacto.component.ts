import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {

  loading: boolean = false
  formulario!: FormGroup;
  mensaje!: string;
  agradecimiento: string = 'Su mensaje fue enviado con éxito';

  constructor(private fb: FormBuilder) { 
    this.crearFormulario();
  }

  crearFormulario(): void{
    this.formulario = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      // telefono: ['', [Validators.required, Validators.minLength(8)]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      comentario: ['', [Validators.required, Validators.minLength(7)]]
    });
  }

  //GET
  get nombreNoValido(){
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched;
  }

  get correoNoValido(){
    return this.formulario.get('correo')?.invalid && this.formulario.get('correo')?.touched
  }

  get comentarioNoValido(){
    return this.formulario.get('comentario')?.invalid && this.formulario.get('comentario')?.touched
  }

  // get telefonoNoValido(){
  //   return this.formulario.get('telefono')?.invalid && this.formulario.get('telefono')?.touched
  // }

  ngOnInit(): void {
  }

  guardar(): void{
    // console.log(this.formulario);
    this.mensaje = this.agradecimiento
    this.LimpiarFormulario()
    setTimeout(() => {
      this.mensaje = ''
    }, 3000);

  }

  LimpiarFormulario(){
    this.formulario.reset();
  }

}
