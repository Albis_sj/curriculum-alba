import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CurriculumComponent } from './components/curriculum/curriculum.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { PortafolioComponent } from './components/portafolio/portafolio.component';
import { EstudiosComponent } from './components/estudios/estudios.component';
import { FooterComponent } from './components/footer/footer.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { ReactiveFormsModule } from '@angular/forms';


import { EditarCitaComponent } from './components/agenda/editar-cita/editar-cita.component';
import { VerCitaComponent } from './components/agenda/ver-cita/ver-cita.component'; // a plugin!



@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavbarComponent,
    CurriculumComponent,
    AgendaComponent,
    PortafolioComponent,
    EstudiosComponent,
    FooterComponent,
    ContactoComponent,
    EditarCitaComponent,
    VerCitaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
